# Youtube Downloader [MP3/MP4]

This is a simple software that allows the user to search for songs and download them. No need to search on youtube, since there is already a dynamic GUI that allows the user to do so in the software itself.

### Prerequisites

Your computer needs to have the .NET framework installed.

## Built With

* [VideoLib](http://www.dropwizard.io/1.0.2/docs/) - The library used to download the videos
* [YoutubeSearch](https://github.com/mrklintscher/YoutubeSearch) - The library used to search for the videos

## Authors

* **David Rossignol**

See also the list of [contributors](https://gitlab.com/davidGroogit/skydownloadv5) who participated in this project.
