﻿using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace SkyMedia
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            HomeManagement.FillHome();
        }

        public void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                WrapPanel wp = wpSearch;
                int index = cbPages.SelectedIndex + 1;
                LibraryManagement.FillSearch(wp);
                YoutHandler.Search(wp, index);
            }
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            YoutHandler.query = txtSearch.Text;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HomeManagement.FillHome();
            LibraryManagement.songVolume = (float)VolumeSlider.Value / 100;
        }

        private void btnLib_Click(object sender, RoutedEventArgs e)
        {
            wpSearch.Children.Clear();
            wpSearch.Height = 0;
            LibraryManagement.FillLibrary(wpSearch);
        }

        private void btnPlaying_Click(object sender, RoutedEventArgs e)
        {

            NowPlaying.fillNowPlaying(wpSearch);
        }

        private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            LibraryManagement.songVolume = (float)VolumeSlider.Value / 100;
            if(LibraryManagement.outputDevice != null)
            {
                LibraryManagement.outputDevice.Volume = LibraryManagement.songVolume;
            }

        }
    }
}
