﻿using MediaToolkit;
using MediaToolkit.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using VideoLibrary;
using YoutubeSearch;

namespace SkyMedia
{
    public class YoutHandler
    {
        public static string query = "";
        public static string url = "";
        public static List<string> urls = new List<string>();
        
        public static void Search(WrapPanel wp, int pages)
        {
            wp.Height = 621;
            urls.Clear();
            var items = new VideoSearch();
            int i = 0;
            double fullHeight = 0;
            
            foreach(var item in items.SearchQuery(query, pages))
            {
                urls.Add(item.Url);
                var request = WebRequest.Create(item.Thumbnail);

                #region Panels
                //Panel creation
                WrapPanel p = new WrapPanel();
                wp.Children.Add(p);
                p.Uid = (i) + "p";
                p.Height = 267;
                p.Width = 350;
                p.Margin = new Thickness(4,5,0,0);
                fullHeight += p.Height;

                if(wp.Height < fullHeight && i % 3 ==0)
                {
                    wp.Height += p.Height;
                }

                Grid gr = new Grid();
                p.Children.Add(gr);
                gr.Width = 336;
                gr.Height = 189;
                gr.Uid = i + item.Title;
                gr.Background = new SolidColorBrush(Colors.ForestGreen);
                gr.MouseEnter += new MouseEventHandler((sender, e) => hoverImage(sender, e, item));
                gr.MouseLeave += new MouseEventHandler((sender, e) => leaveImage(sender, e, item, i));

                //Adding image to panel
                Image img = new Image();
                gr.Children.Add(img);
                img.Uid = i + "i";
                img.Height = 189;
                img.Width = 336;
                img.Stretch = Stretch.Uniform;
                img.Margin = new Thickness(0, 0, 0, 0);
                try
                {
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.UriSource = new Uri(item.Thumbnail, UriKind.RelativeOrAbsolute);
                    bi.EndInit();
                    img.Source = bi;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
                #endregion

                #region Buttons
                //Adding buttons to panel
                Button btn = new Button();
                p.Children.Add(btn);
                btn.Name = "btn" + i;
                btn.Uid = i + "b";
                btn.Content = "Mp3";
                btn.Width = 102;
                btn.FontFamily = new FontFamily("Segoe UI Emoji");
                btn.BorderBrush = new SolidColorBrush(Colors.Transparent);
                btn.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#DDFFFFFF"));
                btn.Margin = new Thickness(7, 15, 0, 0);
                btn.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF939292"));
                btn.Background.Opacity = 0.5;
                btn.Click += new RoutedEventHandler(fetchVideo);

                Debug.WriteLine(btn.Name);

                Button btn1 = new Button();
                p.Children.Add(btn1);
                btn1.Uid = i + "b1";
                btn1.Content = "Mp4";
                btn1.Width = 102;
                btn1.FontFamily = new FontFamily("Segoe UI Emoji");
                btn1.BorderBrush = new SolidColorBrush(Colors.Transparent);
                btn1.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#DDFFFFFF"));
                btn1.Margin = new Thickness(7, 15, 0, 0);
                btn1.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF939292"));
                btn1.Background.Opacity = 0.5;
                btn1.Click += new RoutedEventHandler(fetchVideo);

                Button btn2 = new Button();
                p.Children.Add(btn2);
                btn2.Uid = i + "b2";
                btn2.Content = "Both";
                btn2.Width = 102;
                btn2.FontFamily = new FontFamily("Segoe UI Emoji");
                btn2.BorderBrush = new SolidColorBrush(Colors.Transparent);
                btn2.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#DDFFFFFF"));
                btn2.Margin = new Thickness(7, 15, 0, 0);
                btn2.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF939292"));
                btn2.Background.Opacity = 0.5;
                btn2.Click += new RoutedEventHandler(fetchVideo);
                #endregion

                i++;
            }
        }

        private static void fetchVideo(object sender, RoutedEventArgs e)
        {

            Button btn = (Button)sender;
            var youtube = YouTube.Default;

            string strSubbed = btn.Uid;
            int urlIndex = int.Parse(strSubbed.Substring(0,1));

            //Debug.WriteLine(urlIndex);


            var video = youtube.GetVideo(urls[urlIndex]);

            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic) + "\\" + video.FullName;

            Thread myThread;
            if (btn.Content.ToString() == "Mp3")
            {
                myThread = new Thread(() => mp3Download(path, video));
                myThread.Start();
            }else if(btn.Content.ToString() == "Mp4")
            {
                myThread = new Thread(() => mp4Download(path, video));
                myThread.Start();
            }
            else if (btn.Content.ToString() == "Both")
            {
                myThread = new Thread(() => bothDownload(path, video));
                myThread.Start();
            }

        }

        #region Download Methods
        private static void mp3Download(string path, YouTubeVideo video)
        {
            bothDownload(path, video);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private static void bothDownload(string path, YouTubeVideo video)
        {
            mp4Download(path, video);

            var inputFile = new MediaFile { Filename = path };
            var outputFile = new MediaFile { Filename = $"{path}.mp3" };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                engine.Convert(inputFile, outputFile);
            }
        }

        private static void mp4Download(string path, YouTubeVideo video)
        {
            File.WriteAllBytes(path, video.GetBytes());
        }
        #endregion

        private static void hoverImage(object sender, MouseEventArgs e, VideoInformation item)
        {
            Grid gr = (Grid)sender;
            //Debug.WriteLine("Test Fucked");

            TextBlock lbl = new TextBlock();
            gr.Children.Add(lbl);
            if(item.Title.Length < 50)
            {
                lbl.ContentStart.InsertTextInRun(item.Title);
            }
            else
            {
                lbl.ContentStart.InsertTextInRun(item.Title.Substring(0, 45) + "\n" + item.Title.Substring(45));
                
            }

            lbl.FontFamily = new FontFamily("Segoe UI Emoji");
            lbl.FontSize = 15;
            lbl.Foreground = new SolidColorBrush(Colors.White);
            lbl.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF7D7999"));
            lbl.Background.Opacity = 0.5;
            lbl.Padding = new Thickness(10, 5, 5, 0);
        }

        private static void leaveImage(object sender, MouseEventArgs e, VideoInformation item, int i)
        {
            Grid gr = (Grid)sender;
            gr.Children.Clear();

            //Adding image to panel
            Image img = new Image();
            gr.Children.Add(img);
            img.Uid = i + "i";
            img.Height = 189;
            img.Width = 336;
            img.Stretch = Stretch.Uniform;
            img.Margin = new Thickness(0, 0, 0, 0);
            try
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(item.Thumbnail, UriKind.RelativeOrAbsolute);
                bi.EndInit();
                img.Source = bi;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
    }
}
