﻿using NAudio.Wave;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace SkyMedia
{
    public static class NowPlaying
    {
        public static float currentPosition;
        public static string nowPlayingSong;
        public static void fillNowPlaying(WrapPanel wp)
        {
            
            wp.Children.Clear();

            Grid gr = new Grid();
            wp.Children.Add(gr);
            gr.Height = wp.Width / 7 * 1.5;
            gr.Width = wp.Width;


            Image img = new Image();
            gr.Children.Add(img);
            var uriPlay = new Uri(@"/SkyMedia;component/img/btnPlay.png", UriKind.Relative);
            img.Source = new BitmapImage(uriPlay);
            img.Width = wp.Width / 7;
            img.Height = img.Width;
            img.Margin = new Thickness(15, 15, 15, 0);
            img.VerticalAlignment = VerticalAlignment.Top;
            img.HorizontalAlignment = HorizontalAlignment.Left;

            TextBlock title = new TextBlock();
            gr.Children.Add(title);
            title.FontFamily = new FontFamily("Segoe UI Emoji");
            title.FontSize = 35;
            title.FontWeight = FontWeights.Bold;
            title.Foreground = new SolidColorBrush(Colors.White);
            title.Background = new SolidColorBrush(Colors.Transparent);
            title.Margin = new Thickness(img.Width + 15, 30, 0, 0);
            title.Width = wp.Width - img.Width - 45;
            title.Height = 45;
            title.VerticalAlignment = VerticalAlignment.Top;
            if (nowPlayingSong != "" && nowPlayingSong != null)
            {
                if (nowPlayingSong.Contains("-"))
                    title.ContentStart.InsertTextInRun(nowPlayingSong.Substring(nowPlayingSong.IndexOf("-") + 2));
                else if (nowPlayingSong.Contains("~"))
                    title.ContentStart.InsertTextInRun(nowPlayingSong.Substring(nowPlayingSong.IndexOf("~") + 2));
                else
                    title.ContentStart.InsertTextInRun(nowPlayingSong);
            }else
                title.ContentStart.InsertTextInRun("Nothing playing...");



            Label artist = new Label();
            gr.Children.Add(artist);
            artist.Foreground = new SolidColorBrush(Colors.White);
            artist.Background = new SolidColorBrush(Colors.Transparent);
            artist.FontFamily = new FontFamily("Segoe UI Emoji");
            artist.FontSize = 20;
            artist.Margin = new Thickness(img.Width + 23, title.Height + title.Margin.Top, 0, 0);
            artist.FontWeight = FontWeights.Bold;
            artist.VerticalAlignment = VerticalAlignment.Top;

            if (nowPlayingSong != "" && nowPlayingSong != null)
            {
                if (nowPlayingSong.Contains("-"))
                    artist.Content = nowPlayingSong.Substring(0, nowPlayingSong.IndexOf("-"));
                else if (nowPlayingSong.Contains("~"))
                    artist.Content = nowPlayingSong.Substring(0, nowPlayingSong.IndexOf("~"));
            }else
                artist.Content = "Unkown Artist";
            wp.Height = gr.Height;


            Label startTime = new Label();
            gr.Children.Add(startTime);
            startTime.Foreground = new SolidColorBrush(Colors.White);
            startTime.Background = new SolidColorBrush(Colors.Transparent);
            startTime.Content = LibraryManagement.songLength.ToString().Substring(3, 5);
            startTime.Margin = new Thickness(15, img.Height + 10, 0, 0);

            Slider timeSlider = new Slider();
            gr.Children.Add(timeSlider);
            timeSlider.Maximum = 100f;
            timeSlider.Width = wp.Width - 100;
            if(LibraryManagement.audioFile != null)
            {
                timeSlider.Maximum = LibraryManagement.audioFile.Length;
                currentPosition = (LibraryManagement.audioFile.Position);

                timeSlider.Value = currentPosition;
            }
            else
                timeSlider.Value = 0;
            timeSlider.ValueChanged += ((sender, e) => SetPosition(sender, e, LibraryManagement.audioFile, timeSlider.Value));


            LibraryManagement.FillLibrary(wp);




        }

        public static void SetPosition(object sender, RoutedPropertyChangedEventArgs<double> e, AudioFileReader audioFile, double seconds)
        {
            audioFile.Position = (long)seconds;
        }
    }
}
