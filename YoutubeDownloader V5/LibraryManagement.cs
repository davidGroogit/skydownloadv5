﻿using System;
using System.Diagnostics;
using System.IO;
using NAudio.Wave;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;


namespace SkyMedia
{
    public static class LibraryManagement
    {
        #region Variables
        public static WaveOutEvent outputDevice;
        public static AudioFileReader audioFile;
        public static TimeSpan songLength;
        public static TimeSpan currentPosition;
        public static float songVolume;
        public static string oldPath;
        #endregion

        #region UI Elements

        public static void createUI(WrapPanel wp, double fullHeight, int i, string file)
        {
            WrapPanel p = new WrapPanel();
            wp.Children.Add(p);
            p.Uid = i + "plib";
            string puid = p.Uid;
            p.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF747474"));
            p.Height = 40;
            p.Width = wp.Width;
            p.Margin = new Thickness(4, 5, 0, 0);
            p.Opacity = 0.90;



            DoubleAnimation da = new DoubleAnimation();
            da.From = 10;
            da.To = p.Height;
            da.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            p.BeginAnimation(Button.HeightProperty, da);
            p.MouseEnter += new MouseEventHandler(hoverPanel);
            p.MouseLeave += new MouseEventHandler(leavePanel);

            #region Height Updating
            wp.Height += p.Height +5;
            #endregion

            //Determining the name of the song (If downloaded by the software)
            int strIndex = file.Length - 18;
            string name = file.Substring(strIndex);
            if (name == " - YouTube.mp4.mp3")
            {
                name = file.Substring(0, strIndex);
                for (int j = 0; j < 4; j++)
                {
                    name = name.Substring(name.IndexOf('\\') + 1);
                }

                Debug.WriteLine(name);
            }
            else
                name = file;

            Image btnPlay = new Image();
            p.Children.Add(btnPlay);
            btnPlay.Uid = i + file;
            var uriPlay = new Uri(@"/SkyMedia;component/img/btnPlay.png", UriKind.Relative);
            btnPlay.Source = new BitmapImage(uriPlay);
            btnPlay.Width = 30;
            btnPlay.Height = p.Height - 5;
            btnPlay.Margin = new Thickness(10, 0, 0, 0);
            btnPlay.Opacity = 0.5;
            btnPlay.MouseEnter += new MouseEventHandler(enterPlay);
            btnPlay.MouseLeave += new MouseEventHandler(leavePlay);
            btnPlay.MouseDown += new MouseButtonEventHandler(clickPlay);
            btnPlay.MouseUp += new MouseButtonEventHandler(unclickPlay);

            Image btnStop = new Image();
            p.Children.Add(btnStop);
            btnStop.Uid = i + file;
            var uriStop = new Uri(@"/SkyMedia;component/img/btnStop.png", UriKind.Relative);
            btnStop.Source = new BitmapImage(uriStop);
            btnStop.Width = 30;
            btnStop.Height = p.Height;
            btnStop.Margin = new Thickness(3, 0, 0, 0);
            btnStop.Opacity = 0.5;
            btnStop.MouseEnter += new MouseEventHandler(enterPlay);
            btnStop.MouseLeave += new MouseEventHandler(leavePlay);
            btnStop.MouseDown += new MouseButtonEventHandler(clickStop);
            btnStop.MouseUp += new MouseButtonEventHandler(unclickPlay);

            Label lbl = new Label();
            p.Children.Add(lbl);
            lbl.Uid = i + "lbl";
            lbl.Foreground = new SolidColorBrush(Colors.White);
            lbl.Width = p.Width / 2;
            lbl.Height = p.Height;
            lbl.Content = name;
            lbl.FontFamily = new FontFamily("Segoe UI Emoji");
            lbl.FontSize = 14;
            lbl.Padding = new Thickness(15, 10, 0, 0);

            i++;
        }
        #endregion

        #region Mouse Events
        public static void hoverPanel(object sender, MouseEventArgs e)
        {
            WrapPanel p = (WrapPanel)sender;
            p.Opacity = 1;
        }
        public static void leavePanel(object sender, MouseEventArgs e)
        {
            WrapPanel p = (WrapPanel)sender;
            p.Opacity = 0.90;
        }
        public static void enterPlay(object sender, MouseEventArgs e)
        {
            Image img = (Image)sender;
            img.Opacity = 0.6;

        }
        public static void leavePlay(object sender, MouseEventArgs e)
        {
            Image img = (Image)sender;
            img.Opacity = 0.5;
        }
        private static void clickPlay(object sender, MouseEventArgs e)
        {
            Image img = (Image)sender;
            img.Opacity = 0.5;
            string path = (img.Uid).Substring(1);

            string name = findTitle(path);

            NowPlaying.nowPlayingSong = name;
            var uriPlay = new Uri(@"/YoutubeDownloader V5;component/img/btnPlay.png", UriKind.Relative);
            BitmapImage bimage = new BitmapImage(uriPlay);
            if (img.Source == bimage)
            {
                uriPlay = new Uri(@"/YoutubeDownloader V5;component/img/btnPause.png", UriKind.Relative);
                img.Source = new BitmapImage(uriPlay);
            }

            playSound(path);
        }

        public static void unclickPlay(object sender, MouseEventArgs e)
        {
            Image img = (Image)sender;
            img.Opacity = 0.6;
        }
        public static void clickStop(object sender, EventArgs args)
        {
            outputDevice?.Stop();
        }
        #endregion

        #region Methods
        public static void playSound(string path)
        {
            if(outputDevice == null)
            {
                audioFile = new AudioFileReader(path);
                outputDevice = new WaveOutEvent();
                outputDevice.Init(audioFile);

                outputDevice.PlaybackStopped += OnPlaybackStopped;
            }
            if(outputDevice.PlaybackState != PlaybackState.Playing)
            {
                outputDevice.Play();
            }
            else
            {
                outputDevice?.Pause();
            }
        }

        private static void OnPlaybackStopped(object sender, StoppedEventArgs args)
        {
            outputDevice.Dispose();
            outputDevice = null;
            audioFile.Dispose();
            audioFile = null;
        }

        public static void FillLibrary(WrapPanel wp)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic).ToString();
            string[] filePaths = Directory.GetFiles(path, "*.mp3");

            
            YoutHandler.urls.Clear();

            int i = 0;
            double fullHeight = 0;
            foreach (string file in filePaths)
            {
                createUI(wp, fullHeight, i, file);
            }
        }

        public static void FillSearch(WrapPanel wp)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic).ToString();
            string[] filePaths = Directory.GetFiles(path, "*.mp3");
            string[] files;
            wp.Children.Clear();
            YoutHandler.urls.Clear();

            int i = 0;
            double fullHeight = 0;
            foreach (string file in filePaths)
            {
                string name = findTitle(file);


                if (name.ToLower().Contains(YoutHandler.query.ToLower()))
                createUI(wp, fullHeight, i, file);
            }
        }

        public static string findTitle(string file)
        {
            int strIndex = file.Length - 18;
            string name = file.Substring(strIndex);
            if (name == " - YouTube.mp4.mp3")
            {
                name = file.Substring(0, strIndex);
                for (int j = 0; j < 4; j++)
                {
                    name = name.Substring(name.IndexOf('\\') + 1);
                }
            }
            else
                name = file;

            return name;
        }
        #endregion

    }
}
